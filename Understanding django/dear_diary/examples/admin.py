from django.contrib import admin
from .models import Simple, DateExample

admin.site.register(Simple)
admin.site.register(DateExample)
