from django.db import models
from datetime import date, datetime

class Simple(models.Model):
    text = models.CharField(max_length = 10)
    number = models.IntegerField(null=True)
    url = models.URLField(default = 'examples.com')

    def __str__(self):
        return self.url

class DateExample(models.Model):
    the_date = models.DateTimeField()


class NullExample(models.Model):
    col = models.CharField(max_length=10, blank = True, null=True)

    def __str__(self):
        return self.col
